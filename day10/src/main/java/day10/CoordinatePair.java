package day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CoordinatePair {

	private int x;
	private int y;
	private double angle;
	
	public CoordinatePair(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public double getAngle() {
		return angle;
	}
	
	public void setAngle(double angle) {
		this.angle = angle;
	}
	
	
	public static void main(String[] args) {
		List<String> data = null;
		
		try {
			data = Files.readAllLines(Paths.get("input"));	
		}
		catch(IOException e) {
			
		}
		
		// convert data to 2-dimensional char array
		char[][] array = new char[data.size()][data.get(0).length()];
		
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				array[i][j] = data.get(i).charAt(j);
			}
		}
		
		data.forEach(s->System.out.println(s));
		
		// calculate visibility at each asteroid
		int[][] monitors = calculateMonitorArray(array);
		
		
		// find asteroid with highest visibility
		int target = 0;
		int targetX = 0;
		int targetY = 0;
		
		for (int i = 0; i < monitors.length; i++) {
			for (int j = 0; j < monitors[0].length; j++) {
				if (monitors[i][j] > target) {
					target = monitors[i][j];
					targetX = j;
					targetY = i;
				}
			}
		}
		
		System.out.println("X: " + targetX + " Y: " + targetY + " Visible: " + target);
		
		vaporize(targetX, targetY, array, 200);
	}
	
	
	public static int[][] calculateMonitorArray(char[][] data) {
		int[][] monitorArray = new int[data.length][data[0].length];

		List<CoordinatePair> coordinates = new ArrayList<CoordinatePair>();
		
		
		// find coordinates with an asteroid
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				if (data[i][j] != '.') {
					coordinates.add(new CoordinatePair(j, i));
					
				}
			}
		}
		
		List<CoordinatePair> visibleCoordinates;
		List<Double> angles;
		
		for (CoordinatePair s: coordinates) {
			// copy asteroid coordinates and remove current asteroid
			visibleCoordinates = new ArrayList<CoordinatePair>(coordinates);
			visibleCoordinates.remove(s);
			
			angles = new ArrayList<Double>();
			
			// find angles to other asteroids and count different angles
			for (CoordinatePair c: visibleCoordinates) {
				double angle = Math.toDegrees(Math.atan2(c.getY() - s.getY(), c.getX() - s.getX()));

				if (angle < 0)
					angle += 360;
				
				if (!angles.contains(angle))
					angles.add(angle);
			}
			monitorArray[s.getY()][s.getX()] = angles.size();
			
		}
		
		return monitorArray;
	}
	
	
	public static void vaporize(int x, int y, char[][] data, int nth) {
		List<CoordinatePair> coordinates = new ArrayList<CoordinatePair>();
		CoordinatePair laserLocation = new CoordinatePair(x,y);

		// find coordinates with an asteroid
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				if (data[i][j] != '.') {
					if (!(j == x && i == y))
						coordinates.add(new CoordinatePair(j, i));
				}
			}
		}

		// set laser-asteroid to (0,0)
		for (CoordinatePair c: coordinates) {
			c.setX(c.getX() - laserLocation.getX());
			c.setY(laserLocation.getY() - c.getY());
		}
		
		laserLocation.setX(0);
		laserLocation.setY(0);
		
		List<Double> angles;
		angles = new ArrayList<Double>();
			
		// find and set angles
		for (CoordinatePair s: coordinates) {
			
			// headache
			double angle = Math.toDegrees(Math.atan2(s.getX(), s.getY()));
			
			if (angle < 0)
				angle += 360;

			s.setAngle(angle);
			
			if (!angles.contains(angle))
				angles.add(angle);
		}
		
		Collections.sort(angles);
		
		CoordinatePair target = null;
		int counter = 0;
		
		while (true) { 
			for (double d: angles) {
				if (coordinates.isEmpty())
					return;
				
				// find closest asteroid in current angle
				for (CoordinatePair c: coordinates) {
					if (c.getAngle() == d) {
						if (target == null)
							target = c;
						else
							target = closestAsteroid(laserLocation, c, target);
					}
				}
				
				// asteroid found
				if (target != null) {
					counter++;
					coordinates.remove(target);
				}
				
				if (counter == nth) {
					System.out.printf("Asteroid number %d: X: %d, Y: %d\n", nth, target.getX() + x, y - target.getY());
					return;
				}
				target = null;
			}
		}
	}

	
	public static CoordinatePair closestAsteroid(CoordinatePair base, CoordinatePair A, CoordinatePair B) {
		if ((Math.abs(A.getX() - base.getX()) + Math.abs(A.getY() - base.getY())) <
			(Math.abs(B.getX() - base.getX()) + Math.abs(B.getY() - base.getY())))
			return A;
		else
			return B;
	}
	
}
