package day24;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Part 1 only.
 * 
 */

public class Bugs {

	private List<int[]> states = new ArrayList<int[]>();
	private boolean duplicateStatus = false;
	

	public Bugs(int[] initialState) {
		states.add(initialState);
	}
	
	public int[] getLastState() {
		return states.get(states.size() - 1);
	}
	
	public boolean getDuplicateStatus() {
		return duplicateStatus;
	}
	
	public int getMinutesPassed() {
		return states.size() - 1;
	}
	
	public void cycle() {
		int[] newState = minuteCycle(states.get(states.size() - 1));
		
		for (int[] state: states)
			if (Arrays.equals(state, newState)) {
				duplicateStatus = true;
				break;
			}
		
		states.add(newState);
	}
	
	public int calculateBiodiversity() {
		int[] state = getLastState();
		int c = 0;
		int result = 0;
		
		for (int j = 0; j < 5; j++) {
			for (int i = 4; i >= 0; i--) {
				if (((state[j] >> i) & 1) == 1) {
					result = result | (1 << c); // 2^c, c = 0,1,...,24
				}
				c++;
			}
		}
		
		return result;
	}
	
	public static int[] minuteCycle(int[] state) {
		int[] newState = new int[state.length];
		int zero = 0;

		// first line
		newState[0] = process(state[0], zero, state[1]);

		for (int i = 1; i < state.length - 1; i++) {
			newState[i] = process(state[i], state[i - 1], state[i + 1]);
		}
		
		// last line
		newState[state.length - 1] = process(state[state.length - 1], state[state.length - 2], zero);
		
		return newState;
	}
	
	public static int process(int data, int above, int below) {
		return dies(data, above, below) | infest(data, above, below);
	}
	
	public static int dies(int target, int above, int below) {
		int ones = 31; // "11111" limit to 5 numbers
		int left = target << 1;
		int right = target >> 1;
		int death = (
				((left ^ right) & ~(above | below)) |
				(~(left | right) & (above ^ below))
				);
		
		return death & target & ones;
	}
	
	public static int infest(int target, int above, int below) {
		int ones = 31; // "11111"
		int left = target << 1;
		int right = target >> 1;
		int result = (
				((left ^ right)  & ~(above | below)) | // one neighbour
				(~(left | right) & (above ^ below))  |
				((left & right)  & ~(above | below)) | // two neighbours
				(~(left | right) & (above & below))  |
				((left & above)  & ~(right | below)) |
				((left & below)  & ~(right | above)) | 
				((right & below) & ~(left | above))  |
				((right & above) & ~(left | below))
				) & ~target; // empty cells only
		
		return result & ones;
	}
	
	public static void print(int n) {
		String s = Integer.toBinaryString(n);
		char[] c = new char[5];
		Arrays.fill(c, '.');
		
		for (int i = 0; i < s.length(); i++) {
			c[c.length - s.length() + i] = s.charAt(i) == '0' ? '.' : '#';
		}

		for (char d: c)
			System.out.print(d);
		System.out.println();
	}
	
	public static int[] parseData(String[] dataString) {
		int[] data = new int[dataString.length];
		
		for (int i = 0; i < dataString.length; i++) {
			for (int j = dataString[i].length() - 1; j >= 0; j--) {
				if (dataString[i].charAt(j) == '#') {
					data[i] += 1 << (dataString[i].length() - 1 - j);
				}
			}
		}
		
		return data;
	}
	
	
	public static void main(String[] args) {
		long time = System.nanoTime();
		
		String[] input = null;
		
		try {
			input = Files.readAllLines(Paths.get("input")).toArray(new String[0]);
		}
		catch(IOException e) {
			
		}
		
		int[] intput = parseData(input);
		
		/*
		int[] testData = {Integer.parseInt("00001", 2),
				Integer.parseInt("10010", 2),
				Integer.parseInt("10011", 2),
				Integer.parseInt("00100", 2),
				Integer.parseInt("10000", 2)
		};*/
		
		Bugs bugs = new Bugs(intput);
		
		while(!bugs.getDuplicateStatus()) {
			bugs.cycle();
		}
		
		for (int i: bugs.getLastState())
			print(i);
		
		System.out.println();
		System.out.println("Number of cycles: " + bugs.getMinutesPassed());
		System.out.println("Biodiversity: " + bugs.calculateBiodiversity());
		
		System.out.println("ms : " + (System.nanoTime() - time) / 1000000);
	}
}
