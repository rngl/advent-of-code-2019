package day4;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		String test1 = "111111";
		String test2 = "223450";
		String test3 = "123789";

		System.out.println(validate(test1));
		System.out.println(validate(test2));
		System.out.println(validate(test3));
		
		System.out.println(calculatePasswordCount(256310, 732736));
	
		String test4 = "112233";
		String test5 = "123444";
		String test6 = "111122";
		
		System.out.println(validatePartTwo(test4));
		System.out.println(validatePartTwo(test5));
		System.out.println(validatePartTwo(test6));
		
		System.out.println(calculatePasswordCountPartTwo(256310, 732736));
	}

	
	public static int calculatePasswordCount(int min, int max) {
		
		int count = 0;
		
		for (int i = min; i <= max; i++) {
			if (validate(Integer.toString(i)))
				count++;
		}
		
		return count;
	}
	
	public static boolean validate(String password) {
		
		boolean pair = false;
				
		for (int i = 1; i < password.length(); i++) {
			if (password.charAt(i) < password.charAt(i - 1))
				return false;
			if (password.charAt(i) == password.charAt(i - 1))
				pair = true;
		}
		
		if (pair)
			return true;
		else
			return false;
	}

	
	public static int calculatePasswordCountPartTwo(int min, int max) {
		
		int count = 0;
		
		for (int i = min; i <= max; i++) {
			if (validatePartTwo(Integer.toString(i)))
				count++;
			
		}
		
		return count;
	}
	
	
	public static boolean validatePartTwo(String password) {
		
		int count = 1;
		List<Integer> pairs = new ArrayList<Integer>();
		
		for (int i = 1; i < password.length(); i++) {
			if (password.charAt(i) < password.charAt(i - 1))
				return false;
			if (password.charAt(i) == password.charAt(i - 1)) {
				count++;
			}
			else {
				pairs.add(count);
				count = 1;
			}
		}

		pairs.add(count);

		for (int s: pairs)
			if (s == 2) 
				return true;
		
		return false;
	}
}
