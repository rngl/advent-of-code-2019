package day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Part2 
{
    public static void main( String[] args )
    {
        List<Integer> data = parseDataFile(Paths.get(("input")));
        System.out.println(calculateModuleFuel(14));
        System.out.println(calculateModuleFuel(1969));
        System.out.println(calculateModuleFuel(100756));
        System.out.println(calculateTotalFuel(data));
    }
    
    
    public static List<Integer> parseDataFile(Path path) {
    	
    	List<Integer> outputList = new ArrayList<Integer>();
    	
    	try {
	    	Files.readAllLines(path).forEach(s-> {
	    		outputList.add(Integer.parseInt(s));	
	    	});;
    	}
    	catch(IOException e) {
    		System.out.println("File read failed.");
    	}
    	
    	return outputList;
    }
    
    public static int calculateModuleFuel(int moduleMass) {
    	
    	int fuel = moduleMass / 3 - 2;
    	
    	return fuel <= 0 ? 0 : fuel + calculateModuleFuel(fuel);
    }
    
    public static int calculateTotalFuel(List<Integer> modules) {
    	
    	int sum = 0;
    	
    	for (int s : modules) {
    		sum += calculateModuleFuel(s);
    	}
    	
    	return sum;
    }
   
    
}
