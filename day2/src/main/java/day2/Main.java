package day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {

		int data[] = parseDataFile(Paths.get(("input")));
		
		int test[] = {1,9,10,3,2,3,11,0,99,30,40,50};
		System.out.println(intcode(test)[0]);
		test = new int[]{1,1,1,4,99,5,6,0,99};
		System.out.println(intcode(test)[0]);
		System.out.println(intcode(prepareData(data, 12, 2))[0]);
		
		data = parseDataFile(Paths.get(("input")));
		
		int params[] = findParams(data, 19690720);
		for (int s: params)
			System.out.println(s);
		
	}

	
    public static int[] parseDataFile(Path path) {
    	
    	String[] data = null; 
    	
    	try {
    		data = new String(Files.readAllBytes(path)).split(",");
    	}
    	catch(IOException e) {
    		System.out.println("File read failed.");
    	}
    	
    	int[] output = new int[data.length]; 
    	
    	for (int i = 0; i < data.length - 1; i++) {
    		output[i] = Integer.parseInt(data[i]);
    	}
    	
    	return output;
    }
	
    public static int[] intcode(int[] data) {
    	
    	for (int i = 0; i < data.length - 1; i += 4) {
    		switch(data[i]) {
	    		case 1:
	    			data[data[i+3]] = data[data[i+2]] + data[data[i+1]];
	    			break;
	    		case 2:
	    			data[data[i+3]] = data[data[i+2]] * data[data[i+1]];
	    			break;
	    		case 99:
	    			return data;
    			default:
    				System.out.println("error!");
    				return null;
    		}
    	}
    	return data;
    }
    
    public static int[] prepareData(int[] data, int pos1, int pos2) {
    	data[1] = pos1;
    	data[2] = pos2;
    	
    	return data;
    }
    
    public static int[] findParams(int[] data, int target) {
    	int[] params = new int[2];
    	int[] backup = new int[data.length]; 
    	System.arraycopy(data, 0, backup, 0, data.length);
    	
    	for (int i = 0; i < 100; i++) {
    		for (int j = 0; j < 100; j++) {
    			System.arraycopy(backup, 0, data, 0, data.length);
    			params[0] = i;
    			params[1] = j;
    			
    			if (intcode(prepareData(data, params[0], params[1]))[0] == target)
    				return params;
    		}
    	}
    	
    	return params;
    }
}
