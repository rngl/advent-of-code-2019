package day14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Part 1:
 * create recipes of each line
 * find FUEL recipe
 * cycle through all components in FUEL recipe
 *   replace each component which only appears in its own recipe and in FUEL with its subcomponents
 *     updating amounts
 *   rinse and repeat
 *   only ORE left in FUEL recipe
 * 
 * Part 2:
 * brute force produce fuel until ore amount is above one trillion
 * reset FUEL recipe at the start of the cycle
 * every time fuel component is produced/replaced, save any waste for next cycle
 *   and use any waste from previous cycle 
 *
 * 
 * @author rngl
 *
 */

public class Recipe {
	
	private String id;
	private double amount;
	private double waste = 0;
	private List<String> fuel = new ArrayList<String>();
	private List<Double> fuelMultipliers = new ArrayList<Double>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public void setWaste(double waste) {
		this.waste = waste;
	}
	
	public double getWaste() {
		return waste;
	}

	public void addFuel(String fuel) {
		this.fuel.add(fuel);
	}
	
	public void addFuelMultiplier(double fuelMultiplier) {
		this.fuelMultipliers.add(fuelMultiplier);
	}
	
	public List<String> getFuel() {
		return fuel;
	}

	public void setFuel(List<String> fuel) {
		this.fuel = fuel;
	}

	public List<Double> getFuelMultipliers() {
		return fuelMultipliers;
	}

	public void setFuelMultipliers(List<Double> fuelMultipliers) {
		this.fuelMultipliers = fuelMultipliers;
	}
	
	// replace a component of the recipe with the component's components
	public void replaceWithComponents(Recipe recipe) {
		for (int i = 0; i < fuel.size(); i++) {
			if (fuel.get(i).equals(recipe.getId())) {
				List<Double> fuelAmounts = recipe.produce(fuelMultipliers.get(i));
				
				for (int j = 0; j < recipe.getFuel().size(); j++) {
					String tempFuel = recipe.getFuel().get(j);
					double tempAmount = fuelAmounts.get(j);

					// update amounts if component already exists in recipe
					if (fuel.contains(tempFuel)) {
						for (int k = 0; k < fuel.size(); k++) {
							if (fuel.get(k).equals(tempFuel)) {
								fuelMultipliers.set(k, fuelMultipliers.get(k) + tempAmount);
							}
						}
					}
					else {
						fuel.add(tempFuel);
						fuelMultipliers.add(tempAmount);
					}
				}
				
				fuelMultipliers.remove(i);
				fuel.remove(i);
				break;
			}
		}
	}

	// produces amount of products
	public List<Double> produce(double amount) {
		// use old waste
		amount = amount - waste; 
		double productionMultiplier = Math.ceil((amount / this.amount));
		List<Double> output = new ArrayList<Double>();
		
		// save new waste
		this.waste = this.amount * productionMultiplier - amount;
		
		for (double d: fuelMultipliers) {
			output.add(d * productionMultiplier);
		}
		
		return output;
	}
	

	// start
	public static void main(String[] args) throws IOException {
		List<String> data = Files.readAllLines(Paths.get("input5"));
		List<Recipe> recipes = new ArrayList<Recipe>();
		
		// parse data
		for (String s: data) {
			String[] arrowSplit = s.split(" => ");
			String[] idSplit = arrowSplit[1].split(" ");
			String[] recipeSplit = arrowSplit[0].split(", ");

			Recipe n = new Recipe();
			n.setId(idSplit[1]);
			n.setAmount(Double.parseDouble(idSplit[0]));
			
			for (String d: recipeSplit) {
				idSplit = d.split(" ");
				
				n.addFuel(idSplit[1]);
				n.addFuelMultiplier(Double.parseDouble(idSplit[0]));
			}
			
			recipes.add(n);
		}
		
		double firstFuelCost = 0;
		double sum = 0;
		int fuelAmount = 0;
		
		Recipe fuelRecipe = recipes.stream().filter(s->s.getId().equals("FUEL")).findFirst().get();

		// backups for reset
		List<Recipe> backupRecipes = new ArrayList<Recipe>(recipes);
		List<String> fuelBackup = new ArrayList<String>(fuelRecipe.getFuel());
		List<Double> fuelMultiBackup = new ArrayList<Double>(fuelRecipe.getFuelMultipliers());

		
		while((sum < 1000000000000.0)) {
			// reset recipes
			fuelRecipe.setFuel(new ArrayList<String>(fuelBackup));
			fuelRecipe.setFuelMultipliers(new ArrayList<Double>(fuelMultiBackup));
			recipes = new ArrayList<Recipe>(backupRecipes);
			
			recipes.remove(recipes.stream().filter(s->s.getId().equals("FUEL")).findFirst().get());
			recipes.add(fuelRecipe);
			
			boolean gogo = true;
			
			while(gogo) {
				for (int i = 0; i < fuelRecipe.getFuel().size(); i++ ) {
					// break when only Ore left in the recipe
					if (fuelRecipe.getFuel().size() == 1) {
						gogo = false;
						break;
					}
					int j = i;

					// find recipes which contain the current component
					String tempString = fuelRecipe.getFuel().get(j);
					List<Recipe> testNodes = recipes.stream().filter(s->s.getFuel().contains(tempString)).collect(Collectors.toList());
	
					// do stuff if the only recipe to contain current component is its own
					if (testNodes.size() == 1) {
						Recipe temp = recipes.stream().filter(s->s.getId().equals(tempString)).findFirst().get();
						
						fuelRecipe.replaceWithComponents(temp);
						recipes.remove(temp);
					}
				}
			}
		
			sum += fuelRecipe.getFuelMultipliers().get(0);
			fuelAmount++;
			
			if (firstFuelCost == 0)
				firstFuelCost = sum;

			// progresssss....
			System.out.println("Progress: " + (int) (sum/10000000000.0) + " %");
		}
		
		System.out.println("Total sum : " + sum);
		System.out.println("First fuel cost: " + firstFuelCost);
		System.out.println("Fuel amount: " + fuelAmount); // -1 for result
	}
}
