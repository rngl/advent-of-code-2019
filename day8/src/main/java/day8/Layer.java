package day8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Layer {

	public static void main(String[] args) {

		List<Layer> layers = new ArrayList<Layer>();
		int layerSize = 25 * 6;
		
		Layer temp = null;
		
		try {
			String data = new String(Files.readAllBytes(Paths.get("input")));

			// create layers
			for (int i = 0; i < data.length() / layerSize; i++) {
				temp = new Layer();
				for (int j = 0; j < layerSize; j++) {
					temp.addChar(data.charAt(i * layerSize + j));
				}
				layers.add(temp);
			}
			
			// find right layer
			for (Layer s: layers) {
				if (s.getCount0() < temp.getCount0())
					temp = s;
			}
			
			System.out.println("Result: " + temp.getResult());
			
			Layer resultLayer = new Layer();
			char value;
			char tempValue;
			
			for (int i = 0; i < layerSize; i++) {
				value = '2';
				
				for (int j = 0; j < layers.size(); j++) {
					tempValue = layers.get(j).getChars().get(i); 
					
					if ( tempValue != '2') {
						value = tempValue;
						break;
					}
				}
				
				resultLayer.addChar(value);
			}
			
			for (int i = 0; i < layerSize; i++) {
				if (i % 25 == 0)
					System.out.println();
				
				if (resultLayer.getChars().get(i) == '1')
					System.out.print("#");
				else 
					System.out.print(" ");
				}
		}
		catch(IOException e) {
			
		}
	}

	
	
	private int count0 = 0;
	private int count1 = 0;
	private int count2 = 0;
	private List<Character> chars = new ArrayList<Character>();
	

	public void addChar(char c) {
		
		chars.add(c);
		
		switch(c) {
			case '0':
				count0++;
				break;
			case '1':
				count1++;
				break;
			case '2':
				count2++;
				break;
		}
	}
	
	
	public int getCount0() {
		return count0;
	}
	
	public int getResult() {
		return count1 * count2;
	}
	
	public List<Character> getChars() {
		return chars;
	}
}
