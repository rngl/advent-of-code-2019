package day6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<String> data = parseData(Paths.get("input"));
		System.out.println("Orbit count: " + countOrbits(data));
		System.out.println("Jumps between YOU and SAN: " + findJumpsBetween("YOU", "SAN", data));
	}

	
	
	public static List<String> parseData(Path path) {
		
		List<String> data;
		
		try {
			data = Files.readAllLines(path);
		}
		catch(IOException e) {
			System.out.println("Reading file failed");
			return null;
		}
		
		return data;
	}
	
	
	public static int countOrbits(List<String> data) {
		
		int orbits = 0;
		
		for (String s: data) {
			orbits += countJumps(s, data, "COM");
		}
		
		return orbits;
	}
	
	public static int countJumps(String source, List<String> data, String endIdentifier) {
		
		if (source.startsWith(endIdentifier))
			return 1;
		
		else {
			String temp = data.stream()
					.filter(target -> hasPath(source, target)) 
					.findFirst()
					.map(Object::toString)
					.orElse(null);
			
			return 1 + countJumps(temp, data, endIdentifier);
		}
		
	}
	
	public static boolean hasPath(String source, String target) {
		
		for (int i = 0; i < 3; i++) {
			if (source.charAt(i) != target.charAt(i + 4))
				return false;
		}
		
		return true;
	}
	
	public static int findJumpsBetween(String source, String target, List<String> data) {
		
		String sourceStart = data.stream()
				.filter(s -> s.endsWith(source))
				.findFirst()
				.map(Object::toString)
				.orElse(null);
		
		String targetStart = data.stream()
				.filter(s -> s.endsWith(target))
				.findFirst()
				.map(Object::toString)
				.orElse(null);
		
		List<String> sourceOrbits = new ArrayList<String>();
		List<String> targetOrbits = new ArrayList<String>();
		
		sourceOrbits = getOrbits(sourceStart, "COM", data);
		targetOrbits = getOrbits(targetStart, "COM", data);
		
		// first matching string should be the closest shared system.
		String firstMatch = null;
		
		for (String s : sourceOrbits) { 
			if (targetOrbits.contains(s)) {
				firstMatch = s;
				break;
			}
		}
		
		int jumps1 = countJumps(sourceStart, data, firstMatch.substring(4));
		int jumps2 = countJumps(targetStart, data, firstMatch.substring(4));
		
		return jumps1 + jumps2 - 2;
	}
	
	
	public static List<String> getOrbits(String start, String endIdentifier, List<String> data) {
		
		List<String> orbits = new ArrayList<String>();
		
		while(!start.startsWith(endIdentifier)) {
			start = getNextOrbit(start, data);
			orbits.add(start);
		}
		
		return orbits;
	}
		

	public static String getNextOrbit(String target, List<String> data) {
		return data.stream()
				.filter(s -> hasPath(target, s))
				.findFirst()
				.map(Object::toString)
				.orElse(null);
	}
		
}
