package day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Part2 {

	public static void main(String[] args) {
		
		String data = null;
		
		try {
			data = new String(Files.readAllBytes(Paths.get("input")));
		} catch (IOException e) {
			
		}
		
		int repeats = 10000;
		String offset = data.substring(0,7);
		
		// fill input 10000 times
		int[] input = new int[(data.length() - 1) * repeats];
		for (int j = 0; j < repeats; j++) {
			for (int i = 0; i < data.length() - 1; i++) {
				input[i + (data.length() - 1) * j] = data.charAt(i) - '0';
			}
		}
		
		int[] output = new int[input.length];
		
		// trick puzzle
		// only calculating values for half the array
		// as that part follows a pattern where
		// value[n] = value[n] + value[n+1]
		// and offset > input.size / 2
		for (int k = 0; k < 100; k++) {
			int value = 0;
			
			// calculate from last entry to halfway 
			for (int i = input.length - 1; i >= (input.length ) / 2; i--) {
				value = value + input[i];
				output[i] = value % 10;
			}
			
			// update input for next round
			for (int i = 0; i < output.length; i++) {
				input[i] = output[i];
			}
		}
		
		System.out.println("Offset value: " + offset);
		System.out.print("8 digit message at offset: ");
		
		int offsetvalue = Integer.parseInt(offset);
		for (int i = offsetvalue ; i < (offsetvalue + 8); i++) {
			System.out.print(input[i]);
		}
	}

}
