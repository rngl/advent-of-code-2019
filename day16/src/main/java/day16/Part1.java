package day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Part1 {

	public static void main(String[] args) {
		
		String data = null;
		
		try {
			data = new String(Files.readAllBytes(Paths.get("input")));
		} catch (IOException e) {
			
		}
		
		int[] input = new int[data.length() - 1];
		
		for (int i = 0; i < data.length() - 1; i++) {
			input[i] = data.charAt(i) - '0';
		}
		
		//input = new int[]{1,2,3,4,5,6,7,8};
		//input = new int[]{8,0,8,7,1,2,2,4,5,8,5,9,1,4,5,4,6,6,1,9,0,8,3,2,1,8,6,4,5,5,9,5};
		//input = new int[]{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
		
		int[] output = new int[input.length];
		int[] basePattern = {0, 1, 0, -1};
		int offset = 1;
		
		for (int k = 0; k < 100; k++) {
			for (int i = 1; i < input.length + 1; i++) {
				int value = 0;
				
				for (int j = 0 + offset; j < input.length + offset; j++) {
					value += input[j - offset] * basePattern[(j % (basePattern.length * i)) / i];
				}
				
				output[i - 1] = Math.abs(value) % 10;
			}
			input = output;
		}
	
		System.out.println("Full output:");
		for (int i: output)
			System.out.print(i);
		System.out.println();
	}

}
