package day3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CoordinatePair {

	private int x;
	private int y;
	
	public CoordinatePair(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public static void main(String[] args) {

		List<String> data = null;
		try {
			data = Files.readAllLines(Paths.get("input"));
		}
		catch(IOException e) {
			
		}
		
		List<CoordinatePair> coordinates;
		//coordinates = parseStrings("R75,D30,R83,U83,L12,D49,R71,U7,L72");
		//coordinates = parseStrings("R8,U5,L5,D3");
		//coordinates = parseStrings("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
		coordinates = parseStrings(data.get(0));
		
		List<CoordinatePair> coordinates2;
		//coordinates2 = parseStrings("U62,R66,U55,R34,D71,R55,D58,R83");
		//coordinates2 = parseStrings("U7,R6,D4,L4");
		//coordinates2 = parseStrings("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
		coordinates2 = parseStrings(data.get(1));
		
		CoordinatePair closest = findClosestIntersection(getIntersections(coordinates, coordinates2));
		
		System.out.println("Closest intersection at X: " + closest.getX() + " Y: " + closest.getY());
		System.out.println("Manhattan distance: " + (Math.abs(closest.getX()) + Math.abs(closest.getY())));
		
		List<Integer> pathA = movesToIntersections(coordinates, getIntersections(coordinates, coordinates2));
		List<Integer> pathB = movesToIntersections(coordinates2, getIntersections(coordinates, coordinates2));
		
		int moveCount = pathA.get(0) + pathB.get(0);
		int temp;
		
		for (int i = 0; i < (pathA.size() < pathB.size() ? pathA.size() : pathB.size()); i++) {
			temp = pathA.get(i) + pathB.get(i);
			if (temp < moveCount)
				moveCount = temp;
		}
		
		System.out.println("Shortest amount of moves to an intersection: " + moveCount);
	}
	
	
	public static List<CoordinatePair> getIntersections(List<CoordinatePair> coordinates1, List<CoordinatePair> coordinates2) {
		
		List<CoordinatePair> intersectionCoordinates = new ArrayList<CoordinatePair>();
		
		// loop through coordinate pairs, each pair forms either vertical (x1 = x2) or horizontal line (y1 = y2)
		// horizontal lines only intersect vertical lines
		// compare vertical lines from one list to horizontal lines from the other
		// when intersecting, vertical lines y-coordinate is in range of horizontal lines y values,
		// thus vertical x and horizontal y coordinates form the intersection point
		for (int i = 1; i < coordinates1.size(); i++) {
			if ((coordinates1.get(i - 1).getX() == coordinates1.get(i).getX()) || 
				(coordinates1.get(i - 1).getY() == coordinates1.get(i).getY())) {
				for (int j = 1; j < coordinates2.size(); j++) {
					if (coordinates2.get(j - 1).getY() == coordinates2.get(j).getY()) {
						if (xBetweenAxAndBx(coordinates1.get(i), coordinates2.get(j), coordinates2.get(j - 1)) &&
							yBetweenAyAndBy(coordinates2.get(j), coordinates1.get(i), coordinates1.get(i - 1)))
								intersectionCoordinates.add(new CoordinatePair(coordinates1.get(i).getX(), coordinates2.get(j).getY()));
					}
					else if (coordinates2.get(j - 1).getX() == coordinates2.get(j).getX()) {
						if (xBetweenAxAndBx(coordinates2.get(j), coordinates1.get(i), coordinates1.get(i - 1)) &&
							yBetweenAyAndBy(coordinates1.get(i), coordinates2.get(j), coordinates2.get(j - 1)))
								intersectionCoordinates.add(new CoordinatePair(coordinates2.get(j).getX(), coordinates1.get(i).getY()));
					}
				}
			}
		}
		
		return intersectionCoordinates;
	}

	public static boolean yBetweenAyAndBy(CoordinatePair y, CoordinatePair A, CoordinatePair B) {
		if (((y.getY() <= B.getY()) && (y.getY() >= A.getY())) ||
			((y.getY() >= B.getY()) && (y.getY() <= A.getY()))) {
			return true;
		}
		
		return false;
	}
	
	public static boolean xBetweenAxAndBx(CoordinatePair x, CoordinatePair A, CoordinatePair B) {
		if (((x.getX() <= B.getX()) && (x.getX() >= A.getX())) ||
			((x.getX() >= B.getX()) && (x.getX() <= A.getX()))) {		
			return true;
		}
		
		return false;
	}
	
	public static CoordinatePair findClosestIntersection(List<CoordinatePair> intersectionCoordinates) {
		CoordinatePair result = null;
		
		for (CoordinatePair c: intersectionCoordinates) {
			if (!(c.getX() == 0 && c.getY() == 0)) {
				if (result == null)
					result = c;
				else {
					if ((Math.abs(c.getX()) + Math.abs(c.getY())) < (Math.abs(result.getX()) + Math.abs(result.getY())))
						result = c;
				}
			}
		}
		
		return result;
	}
	

	public static List<Integer> movesToIntersections(List<CoordinatePair> pathCoordinates, List<CoordinatePair> intersections) {

		int count = 0;
		int x;
		int y;
		List<Integer> moveCount = new ArrayList<Integer>();
		
		for (int j = 1; j < intersections.size(); j++) {
		x = intersections.get(j).getX();
		y = intersections.get(j).getY();

			// find the lines where the intersections are
			for (int i = 1; i < pathCoordinates.size(); i++) { 
				if (pathCoordinates.get(i).getX() == x) {
					if (((pathCoordinates.get(i - 1).getY() <= y && pathCoordinates.get(i).getY() >= y)) ||
						 (pathCoordinates.get(i - 1).getY() >= y && pathCoordinates.get(i).getY() <= y)) {
						count = countFullMoves(pathCoordinates, i);
						count += Math.abs(pathCoordinates.get(i - 1).getY() - y); 
						moveCount.add(count);
					}
				}
				else if (pathCoordinates.get(i).getY() == y) {
					if (((pathCoordinates.get(i - 1).getX() <= x && pathCoordinates.get(i).getX() >= x)) ||
						 (pathCoordinates.get(i - 1).getX() >= x && pathCoordinates.get(i).getX() <= x)) {
						count = countFullMoves(pathCoordinates, i);
						count += Math.abs(pathCoordinates.get(i - 1).getX() - x);
						moveCount.add(count);
					}
				}
			}
		}
		return moveCount;
	}
	
	// count the amount of moves needed to reach the point before intersection
	public static int countFullMoves(List<CoordinatePair> coordinates, int lines) {
		
		int count = 0;
		
		for (int i = 1; i < lines; i++) {
			count += Math.abs(coordinates.get(i).getX() - coordinates.get(i - 1).getX()) + 
					Math.abs(coordinates.get(i).getY() - coordinates.get(i - 1).getY());
		}
		
		return count;
	}
	
	// parse input data to coordinates
	public static List<CoordinatePair> parseStrings(String data) {
		List<CoordinatePair> coordinates = new ArrayList<CoordinatePair>();
		String[] splitData = data.split(",");
		
		CoordinatePair start = new CoordinatePair(0,0);
		coordinates.add(start);
		
		for (String s: splitData) {
			
			switch(s.substring(0, 1)) {
				case "U":
					start = new CoordinatePair(start.getX(), start.getY() + Integer.parseInt(s.substring(1)));
					break;
				case "D":
					start = new CoordinatePair(start.getX(), start.getY() - Integer.parseInt(s.substring(1)));
					break;
				case "L":
					start = new CoordinatePair(start.getX() - Integer.parseInt(s.substring(1)), start.getY());
					break;
				case "R":
					start = new CoordinatePair(start.getX() + Integer.parseInt(s.substring(1)), start.getY());
					break;
			}
			coordinates.add(start);
		}
		
		return coordinates;
	}
}
